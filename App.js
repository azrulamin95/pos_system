import React from 'react';
import 
{ 
  createAppContainer, 
  createSwitchNavigator
} from 'react-navigation';
import { Dimensions, StatusBar } from "react-native";
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';

/*Navigations*/
import MenuDrawer from "./src/components/navigations/MenuDrawer";
import MenuButton from "./src/components/navigations/MenuButton";

/*Screen*/
import AccountScreen from './src/screens/setting/AccountScreen';
import SignupScreen from './src/screens/auth/SignUpScreen';
import SigninScreen from './src/screens/auth/SignInScreen';
import PaymentScreen from "./src/screens/mainFlow/PaymentScreen";
import ProductListScreen from "./src/screens/mainFlow/ProductListScreen";
import OpenCounterScreen from "./src/screens/mainFlow/OpenCounterScreen";
//import { Icon } from 'react-native-vector-icons/Icon';

const WIDTH = Dimensions.get('window').width;

const DrawerConfig = {
  drawerWidth : WIDTH * 0.83, 
  contentComponent :({navigation}) => {
    return (
      <MenuDrawer navigation={navigation}/>
    )
  }
}

const SignUpNavigator = createStackNavigator({
    Signup: SignupScreen,
  },{
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#4388CD',
      },
      headerTitle: 'CHECKOUT',
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
        textAlign: 'center',
        flex: 1
      },
      headerLeft: (
        <MenuButton navigation={navigation}/>
      )
    },
});

const SignInNavigator = createStackNavigator({
  Signin: SigninScreen,
},{
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#4388CD',
    },
    headerTitle: 'SIGN IN',
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
      textAlign: 'center',
      flex: 1
    },
    //headerLeft:(
      // <Icon
      //   style={{paddingLeft:10, color:'white'}}
      //   onPress={() => navigation.openDrawer()}
      //   name="md-menu"
      //   size={30}
      // />
    //)
  },
});

const AccountNavigator = createStackNavigator({
  Account: AccountScreen,
},{
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: '#4388CD',
    },
    headerTitle: 'ACCOUNT',
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
      textAlign: 'center',
      flex: 1
    },
    //headerLeft:(
      // <Icon
      //   style={{paddingLeft:10, color:'white'}}
      //   onPress={() => navigation.openDrawer()}
      //   name="md-menu"
      //   size={30}
      // />
    //)
  },
});

const MyDrawerNavigator = createDrawerNavigator(
  {
    Signup: SignUpNavigator,
    Signin: SignInNavigator,
    Account: AccountNavigator,
  },{
    hideStatusBar: true,
    drawerBackgroundColor: '#4388CD',
    contentOptions:{
      activeTintColor: '#fff',
    }
  }
);

// const MyStackNavigator = createStackNavigator({
//   drawerFlow: MyDrawerNavigator,
//   OpenCounter: OpenCounterScreen,
// },{
//   defaultNavigationOptions: {
//     headerStyle: {
//       backgroundColor: '#f4511e',
//     },
//     headerTintColor: '#fff',
//     headerTitleStyle: {
//       fontWeight: 'bold',
//     },
//   },
//   headerLayoutPreset: 'center' 
// });

// const switchNavigator = createSwitchNavigator({
//   defaultHome: MyStackNavigator,
//   loginFlow: createStackNavigator({
//     Signin: SigninScreen,
//     Signup: SignupScreen
//   }),
//   mainFlow: createBottomTabNavigator({
//     trackListFlow: createStackNavigator({
//       Payment: PaymentScreen,
//       ProductList: ProductListScreen
//     })    
//   })
// });

const App = createAppContainer(MyDrawerNavigator);

export default () => {
  return (
    <App />
  );
};