import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, Image } from "react-native";
import { Input, Button } from 'react-native-elements';
import CounterForm from "../../components/navigations/CounterForm";

const { width, height } = Dimensions.get('window');

const OpenCounterScreen = ({navigation}) => {
    const imageOne = {
        one: require('../../../assets/100.jpg'),
        two: require('../../../assets/50.jpg')
    };

    const imageTwo = {
        one: require('../../../assets/20.jpg'),
        two: require('../../../assets/10.jpg')
    };

    const imageThree = {
        one: require('../../../assets/5.jpg'),
        two: require('../../../assets/1.jpg')
    };
    return (
        <View style={styles.container}>
            <CounterForm 
                imageLink={imageOne}
            />
            <CounterForm 
                imageLink={imageTwo}
            />
            <CounterForm 
                imageLink={imageThree}
            />
            <View style={{marginTop: 25}}>
                <TouchableOpacity onPress={() => navigation.navigate('drawerFlow')}>
                    <View style={styles.button}>
                        <Text style={{fontSize: 16, fontWeight: 'bold'}}>CONTINUE</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View style={styles.button_skip}>
                        <Text style={{fontSize: 16, fontWeight: 'bold'}}>SKIP</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
    },
    container_top: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        marginTop:  20,
        marginHorizontal: 15
    },
    container_bottom: {

        marginHorizontal: 15,
        marginBottom: 20
    },
    text:{
        fontSize: 30
    },
    box:{
        height: 100,
        width: 160,
        backgroundColor: '#D3D3D3',
        marginVertical: 10,
        resizeMode: 'stretch'
    },
    box_input:{
        flexDirection: 'row'
    },
    button: {
        backgroundColor: '#4388CD',
        height: 50,
        width: width - 40,
        marginHorizontal: 20,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 5,
        shadowOffset: {width:2, height: 2},
        shadowColor: 'black',
        shadowOpacity: 0.2
    },
    button_skip: {
        backgroundColor: 'white',
        height: 50,
        width: width - 40,
        marginHorizontal: 20,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 5,
        shadowOffset: {width:2, height: 2},
        shadowColor: 'black',
        shadowOpacity: 0.2,
        borderColor: 'black',
        borderStyle: 'solid',
        borderWidth: 1
    }
});

OpenCounterScreen.navigationOptions = () => {
    return {
        title: 'OPEN COUNTER',
        headerStyle: {
            backgroundColor: '#4388CD',
        },
        headerTitleStyle: {
            color: 'white'
        }
    };
};

export default OpenCounterScreen;