import React from "react";
import { StyleSheet, Text, View } from 'react-native';
import MenuButton from "../../components/navigations/MenuButton";

const Account = ({navigation}) => {
    return <View style={styles.container}>
        <MenuButton navigation={navigation}/>
        <Text style={styles.text}>Account Screen</Text>
    </View>
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    text:{
        fontSize: 30
    }
});

export default Account;