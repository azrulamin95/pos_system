import React from "react";
import { ActivityIndicator, StyleSheet, View } from "react-native";

const LoaderScreen = () => {
    return (
        <View>
            <ActivityIndicator size="large" color="#ff0000"/>
        </View>
    );
}

const style = StyleSheet.create({

});

export default LoaderScreen;