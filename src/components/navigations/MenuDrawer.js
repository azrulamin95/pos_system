import React from 'react';
import {
    Dimensions, 
    StyleSheet, 
    View, 
    Text, 
    TouchableOpacity, 
    Image, 
    ScrollView,
    StatusBar
} from "react-native";
import { Feather, Ionicons } from '@expo/vector-icons';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const MenuDrawer = ({navigation}) => {
    const navLink = (nav,text,icon) => {
        return (
            <TouchableOpacity style={{height: 40}} onPress={() => {navigation.navigate(nav)}}>
                <View style={{flexDirection: "row",justifyContent: 'center',alignItems: 'center',marginLeft: 20}}>
                    {icon == 'ios-person' || icon == 'ios-cart' || icon == 'md-analytics' || icon == 'md-settings' || icon == 'ios-help-circle-outline' ? 
                        <Ionicons 
                            name={icon}
                            size={20}
                            style={{color: 'white'}}
                        /> :
                        <Feather 
                            name={icon}
                            size={20}
                            style={{color: 'white'}}
                        />
                    }
                    <Text style={styles.links}>{text}</Text>
                </View>
            </TouchableOpacity>
        )
    };

    return (
        <View style={styles.container}>
            <StatusBar hidden={true} />
            <ScrollView style={styles.scroller}>
                <View style={styles.profile}>
                    <Text style={styles.link}>BUSINESS NAME ></Text>
                </View>
                <View style={styles.bottomLinks}>
                    {navLink('OpenCounter', 'CHECKOUT', 'check-circle')}
                    {navLink('Signup', 'PRODUCTS', 'shopping-bag')}
                    {navLink('Account', 'CUSTOMERS', 'ios-person')}
                    {navLink('loginFlow', 'TRANSACTIONS', 'ios-cart')}
                    {navLink('loginFlow', 'ANALYTICS', 'md-analytics')}
                    {navLink('loginFlow', 'USERS', 'users')}
                    {navLink('loginFlow', 'SETTING', 'md-settings')}
                    {navLink('loginFlow', 'HELP', 'ios-help-circle-outline')}
                </View>
                <View style={styles.footer}>
                    <Text style={styles.description}>T.I.M POS System</Text>
                    <Text style={styles.version}>v1.0</Text>
                </View>
            </ScrollView>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex :1,
        backgroundColor: '#4388CD',
        color: 'white'
    },
    scroller:{
        flex: 1,
        backgroundColor: '#4388CD'
    },
    profile:{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 25
    },
    profileText:{
        flex: 3,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    name:{
        fontSize: 20,
        paddingBottom: 5,
        color: 'white',
        textAlign: 'left'
    },
    imgView:{
        flex: 3,
        paddingLeft: 20,
        paddingRight: 20
    },
    img:{
        height: 70,
        width: 70,
        borderRadius: 50
    },
    topLinks:{
        height: 160,
        backgroundColor: 'black',
    },
    bottomLinks: {
        flex: 1,
        paddingTop: 40,
        paddingBottom: 450,
    },
    link: {
        flex: 1,
        fontSize: 20,
        padding: 6,
        paddingLeft: 25,
        margin: 5,
        textAlign: 'left',
        color: 'white'
    },
    links: {
        flex: 1,
        fontSize: 16,
        padding: 6,
        paddingLeft: 14,
        margin: 5,
        textAlign: 'left',
        color: 'white'
    },
    footer:{
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        borderTopWidth: 1,
        borderTopColor: 'lightgray'
    },
    description:{
        flex: 1,
        marginLeft: 20,
        fontSize: 16
    },
    version:{
        flex: 1,
        textAlign: 'right',
        marginRight: 20,
        color: 'gray'
    }
});

export default MenuDrawer;