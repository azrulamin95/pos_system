import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { Input, Button } from 'react-native-elements';

const CounterForm = ({imageLink}) => {
    return (
        <View>
            <View style={styles.container_top}>
                <Image style={styles.box} source={imageLink.one} />
                <Image style={styles.box} source={imageLink.two} />
            </View>
            <View style={{flexDirection: "row", marginHorizontal: 15}}>
                <View style={{marginLeft:5}}></View>
                <View style={styles.box_1}>
                    <Button title="+"/>
                    <Input
                        value="0"
                        autoCapitalize="none"
                        autoCorrect={false}
                    />
                    <Button title="-"/>
                </View>
                <View style={{marginLeft:70}}></View>
                <View style={styles.box_1}>
                    <Button title="+"/>
                    <Input
                        value="0"
                        autoCapitalize="none"
                        autoCorrect={false}
                    />
                    <Button title="-" style={{width: 40}} />
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container_top: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        marginHorizontal: 15,
    },
    container_bottom: {
        marginHorizontal: 15,
        marginBottom: 20
    },
    box:{
        height: 85,
        width: 160,
        marginVertical: 10,
        resizeMode: 'stretch',
        flexDirection: 'row'
    },
    box_1:{
        width: 100,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
});

export default CounterForm;
